# pensamento-do-dia
## Suporta Joomla 2.5 e 3.x

Pequeno módulo que mostra um pensamento aleatório a cada visita. São uns 200 pensamentos no total.

## Instalação
https://github.com/ribafs/pensamento-do-dia

Demo
http://ribafs.org/joomlademo/
